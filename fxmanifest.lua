fx_version 'cerulean'
games { 'gta5' }

author 'Vladimir Mateev'
description 'Desmontar armas'
version '2.1.0'

client_scripts {
    'config.lua',
    '@es_extended/locale.lua',
    'client/client.lua',
}

server_scripts {
    'config.lua',
    '@es_extended/locale.lua',
    'server/server.lua',
}
