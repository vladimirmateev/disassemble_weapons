local ESX = nil
TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

ESX.RegisterServerCallback('MTCore:getLoadout', function(playerId, cb)
  local xPlayer = ESX.GetPlayerFromId(playerId)
  cb(xPlayer.getLoadout())
end)

RegisterNetEvent('MTCore:disassemble')
AddEventHandler('MTCore:disassemble', function(weaponID, weaponAMMO)
  local xPlayer = ESX.GetPlayerFromId(source)

  if xPlayer ~= nil and xPlayer.hasWeapon(weaponID) then
    if Config.AllowedWeapons[weaponID] then
      if not Config.WhiteWeapons[weaponID] then
        if xPlayer.canCarryItem(weaponID, 1) and xPlayer.canCarryItem('m'..weaponID, weaponAMMO) then
          -- Remove weapon from player
          xPlayer.removeWeapon(weaponID)
          xPlayer.removeWeaponAmmo(weaponID, weaponAMMO)
          -- Add the item correspond to the weapon and its bullets
          xPlayer.addInventoryItem(weaponID, 1)
          xPlayer.addInventoryItem('m'..weaponID, weaponAMMO)    
          -- Discord log
          sendToDiscord(Config.Name, '['..xPlayer.getName()..' ('..xPlayer.getIdentifier()..')] Ha desmontado una '..weaponID..' y '..weaponAMMO..' balas')
        else
          xPlayer.showNotification(Config.Locale['INVALID-WEIGHT'])
        end
      else
        if xPlayer.canCarryItem(weaponID, 1) then
          -- Remove weapon from player
          xPlayer.removeWeapon(weaponID)
          -- Add the item correspond to the white weapon
          xPlayer.addInventoryItem(weaponID, 1)
          -- Discord log
          sendToDiscord(Config.Name, '['..xPlayer.getName()..' ('..xPlayer.getIdentifier()..')] Ha desmontado una '..weaponID)
        else
          xPlayer.showNotification(Config.Locale['INVALID-WEIGHT'])
        end
      end
    else
      xPlayer.showNotification(Config.Locale['ERROR-DISSASEMBLY'])
    end
  end
end)

for k,v in pairs(Config.AllowedWeapons) do
  ESX.RegisterUsableItem(k, function(playerId)
    local xPlayer = ESX.GetPlayerFromId(playerId)

    if xPlayer ~= nil and not xPlayer.hasWeapon(k) then
      xPlayer.removeInventoryItem(k, 1)
      xPlayer.addWeapon(k, 0)
      sendToDiscord(Config.Name, '['..xPlayer.getName()..' ('..xPlayer.getIdentifier()..')] Ha montado una '..k)
    else
      xPlayer.showNotification(Config.Locale['ALREADY-HAVE'])
    end
  end)
  
  ESX.RegisterUsableItem('m'..k, function(playerId)
    local xPlayer = ESX.GetPlayerFromId(playerId)
    
    if xPlayer ~= nil then
      if xPlayer.hasWeapon(k) then
        local ammo = xPlayer.getInventoryItem('m'..k).count
        xPlayer.removeInventoryItem('m'..k, ammo)
        xPlayer.addWeaponAmmo(k, ammo)
        sendToDiscord(Config.Name, '['..xPlayer.getName()..' ('..xPlayer.getIdentifier()..')] Ha montado '.. ammo..' balas en una '..k)
      else
        xPlayer.showNotification(Config.Locale['FIRST-DISSASEMBLY'])
      end
    end
  end)
end

function sendToDiscord(name, message)
  if message == nil or message == '' then return FALSE end
  PerformHttpRequest(Config.Webhook, function(err, text, headers) end, 'POST', json.encode({username = name, content = '```css\n'..message..'```'}), { ['Content-Type'] = 'application/json' })
end